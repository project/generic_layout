Generic Layout
============

# DESCRIPTION
Generic Layout is a simple module that provides a layout plugin which can dynamically define new regions using css grid template columns and areas.

The project was initialy inspired by https://www.drupal.org/project/generic_layout but this project is not maintained and merge request are not handled.

# REQUIREMENTS
This module requires Layout Discovery modules of Drupal core.

# INSTALLATION
Install the Grid Layout module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

# ROADMAP / FEATURE SKETCHBOOK

Generalize GenericLayoutLibraryManager:
### Similar functionality in other modules
- drupal/webform: see 'entity.webform.settings_assets'


MAINTAINERS
------------
- Thomas Sommer (TomSaw) - https://www.drupal.org/u/tomsaw
