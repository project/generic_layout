<?php
declare(strict_types=1);

namespace Drupal\generic_layout\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

use Sabberworm\CSS\Value\Size;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class AutoCompleteController extends ControllerBase {

  const KEYWORDS = ['none', 'auto', 'initial', 'inherit', 'min-content', 'max-content', 'fit-content'];
  const FUNCTIONS = ['minmax'];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(

    );
  }

  // @todo check how Drupal\system\Controller\EntityAutocompleteController works
  public function handleAxis(Request $request): JsonResponse
  {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = explode(' ', $input);
    $end = &$input[count($input) - 1];

    $end_untouched = $end;
    if(is_numeric($end_untouched)) {
      foreach(Size::RELATIVE_SIZE_UNITS + Size::ABSOLUTE_SIZE_UNITS as $unit) {
        $end = $end_untouched . $unit;
        $results[] = join(' ', $input);
      }
    }
    elseif(str_starts_with($end, 'mi')) {
      foreach(['min', 'minmax'] as $value) {
        $end = $value;
        $results[] = join(' ', $input);
      }
    }

    return new JsonResponse($results);
  }
}
