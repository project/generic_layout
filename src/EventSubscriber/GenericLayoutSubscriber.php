<?php
declare(strict_types=1);

namespace Drupal\generic_layout\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Event\PrepareLayoutEvent;

use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\Core\Asset\LibraryDiscoveryCollector;

use Drupal\generic_layout\GenericLayoutManager;
use Drupal\generic_layout\GenericLayoutHelper;
use Drupal\generic_layout\Plugin\Layout\GenericLayout;

class GenericLayoutSubscriber implements EventSubscriberInterface {

  public function __construct(
    protected readonly LayoutTempstoreRepositoryInterface $layoutTempstoreRepository,
    protected readonly LibraryDiscoveryCollector $collector
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      LayoutBuilderEvents::PREPARE_LAYOUT => 'onPrepareLayout'
    ];
  }

  /**
   * Mimic the schema usually created by LibraryDiscoveryparser::buildByExtension()
   */
  public function getRenderedLibrary(GenericLayout $layout): array {
    $file_uri = GenericLayoutManager::getCssUri($layout);

    return [
      'css' => [
        0 => [
          'preprocess' => false,
          'weight' => CSS_LAYOUT,
          'group' => 'generic_layout',
          'type' => "file",
          'data' => \Drupal::service('file_url_generator')->generateString($file_uri),
          'version' => -1
        ]
      ],
      'js' => [],
      'dependencies' => [
        'generic_layout/generic_layout'
      ],
      'license' => []
    ];
  }

  /**
   * Prepares a layout for use in the UI.
   *
   * @param \Drupal\layout_builder\Event\PrepareLayoutEvent $event
   *   The prepare layout event.
   */
  public function onPrepareLayout(PrepareLayoutEvent $event) {
    $section_storage = $event->getSectionStorage();

    if ($this->layoutTempstoreRepository->has($section_storage)) {
      $sections = $this->layoutTempstoreRepository->get($section_storage)->getSections();
      $gl_sections = GenericLayoutHelper::extractGenericLayoutSections($sections);

      $libraries = $this->collector->get('generic_layout');
      foreach ($gl_sections as $section) {
        $layout = $section->getLayout();
        $libraries[$layout->getInstance()] = $this->getRenderedLibrary($layout);
      }
      $this->collector->set('generic_layout', $libraries);
    }
  }
}
