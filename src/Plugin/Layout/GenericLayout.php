<?php
declare(strict_types=1);

namespace Drupal\generic_layout\Plugin\Layout;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Sabberworm\CSS\Rule\Rule;
use Drupal\breakpoint\Breakpoint;

use Drupal\Component\Utility\Html;

use Sabberworm\CSS\Comment\Comment;
use Sabberworm\CSS\Value\CSSString;
use Sabberworm\CSS\CSSList\Document;
use Drupal\Core\Layout\LayoutDefault;
use Sabberworm\CSS\Property\Selector;

use Drupal\ui_styles\Definition\StyleDefinition;

use Sabberworm\CSS\Value\RuleValueList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ui_styles\StylePluginManager;

use Sabberworm\CSS\CSSList\AtRuleBlockList;
use Sabberworm\CSS\RuleSet\DeclarationBlock;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\generic_layout\GenericLayoutHelper;
use Drupal\generic_layout\GenericLayoutManager;
use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\ui_styles\Source\SourcePluginManager;
use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Layout\Icon\IconBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GenericLayout extends LayoutDefault implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  // Althought this describes css grid "areas", Drupal users are used for "Regions"
  const area_label_singular = 'Region';
  const area_label_plural = 'Regions';

  protected array $breakpoint_group;

  const CONFIGS = ['query', 'breakpoints'];

  const GRID_AXES = ['columns', 'rows'];
  
  const GRID_FLOW = ['align', 'justify'];
  protected array $grid_flow_values = [
    'content' => [
      'start' => [
        'label' => 'Start',
      ],
      'center' => [
        'label' => 'Center',
      ],
      'end' => [
        'label' => 'End',
      ],
      'space-between' => [
        'label' => 'Space Between',
      ],
      'space-around' => [
        'label' => 'Space Around',
      ],
      'space-evenly' => [
        'label' => 'Space Evenly',
      ]
    ],
    'items' => [
      'start' => [  
        'label' => 'Start',
      ],
      'center' => [
        'label' => 'Center',
      ],
      'end' => [
        'label' => 'End',
      ],
      'stretch' => [
        'label' => 'Stretch (default)',
      ]
    ]
  ];

  /**
   * GenericLayout constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, BreakpointManagerInterface $breakpoint_manager,
    protected readonly GenericLayoutManager $layoutManager,
    protected readonly ExtensionPathResolver $extensionPathResolver,
    protected readonly SourcePluginManager $sourcePluginManager,
    protected readonly IconBuilderInterface $iconBuilder,
    protected readonly LibraryDiscoveryCollector $libraryDiscoveryCollector,
    protected $messenger
  ) {
    $this->breakpoint_group = $breakpoint_manager->getBreakpointsByGroup(
      $config_factory->get('generic_layout.settings')->get('breakpoint_group')
    );

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): GenericLayout {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('breakpoint.manager'),
      $container->get('generic_layout.manager'),
      $container->get('extension.path.resolver'),
      $container->get('plugin.manager.ui_styles.source'),
      $container->get('layout.icon_builder'),
      $container->get('library.discovery.collector'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration() + ['query' => 'media'];

    // Add a "default" region to start with
    if($this->isNew()) {
      $breakpoint_id_first = str_replace('.', '/', reset($this->breakpoint_group)->pluginId);
      $config['breakpoints'][$breakpoint_id_first]['areas'] = [["default"]];
    }

    return $config;
  }

  private static function formItemReference(string $breakpoint_id, string $name): string {
    return implode('][', ['breakpoints', $breakpoint_id, $name]);
  }

  private function getAxisFormItems(string $axis, string $prop, array|null $defaults, string $areas_selector): array {
    $axis_form = [
      '#type' => 'fieldset',
      '#states' => [
        'invisible' => [
          $areas_selector => ['value' => ''],
        ]
      ],
      '#attributes' => [
        'class' => ['axis']
      ]
    ];

    $axis_form[$axis] = [
      '#type' => 'textfield',
      '#title' => ucfirst($axis),
      // '#description' => $this->t("Follow the css standard for grid-template-{$axis} property."),
      '#default_value' => join(' ', $defaults[$axis] ?? []),
      '#placeholder' => '1fr 2fr',
      '#autocomplete_route_name' => "generic_layout.autocomplete.axis"
    ];

    $toolbar = $this->sourcePluginManager->createInstance('toolbar');

    foreach ($this->grid_flow_values as $type => &$values) {
      # Generate icon paths if not yet done
      if(!isset(array_key_first($values)['icon'])) {
        $generic_layout_assets = $this->extensionPathResolver->getPath('module', 'generic_layout') . '/assets';
        foreach($values as $key => &$value)
          $value['icon'] = $generic_layout_assets . "/{$prop[0]}{$type[0]}-{$key}.png";
      }
      
      $property = "{$prop}_{$type}";

      $definition = new StyleDefinition([
        'label' => ucfirst($type),
        'options' => $values
      ]);
      $axis_form[$property] = $toolbar->getWidgetForm($definition, $defaults[$property] ?? '');
    }

    return $axis_form;
  }

  protected function getBreakpointTitle(Breakpoint $breakpoint): array {
    if ($query = $breakpoint->getMediaQuery()) {
      $title = $breakpoint->getLabel() . ' ' . str_replace('all and ', '', $query);
    } else {
      $title = 'all';
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'legend',
      '#value' => $title
    ];
  }

  protected function getBreakpointIcon(Breakpoint $breakpoint, array|null $defaults): array {
    if (isset($defaults['areas'])) {
      $icon_width = 50;

      if ($query = $breakpoint->getMediaQuery()) {
        $query_width = intval(filter_var($query, FILTER_SANITIZE_NUMBER_INT));
        // clamp() does not exist
        $icon_width = min(max(40, $query_width / 5), 340);
      }

      $this->iconBuilder->setHeight(count($defaults['areas']) * 20);
      $this->iconBuilder->setWidth($icon_width);

      return $this->iconBuilder->build($defaults['areas']);
    }

    return [];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['#attributes']['class'][] = 'generic-layout-settings';

    if (!$this->breakpoint_group) {
      $message = $this->t(
        "Generic Layout Configuration is missing! Visit @link to create a configuration.",
        ['@link' => Link::createFromRoute('Generic Layout Settings', 'generic_layout.settings_form')->toString()]
      );

      $this->messenger->addError($message);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];

      return $form;
    }

    if (empty($this->breakpoint_group)) {
      $message = $this->t(
        "Breakpoint Group <em>@bp_group</em> is unavailable. Visit @link_config to update your configuration or @link_themes.",
        [
          '@bp_group' => $this->breakpoint_group,
          '@link_config' => Link::createFromRoute('Generic Layout Settings', 'generic_layout.settings_form')->toString(),
          '@link_themes' => Link::createFromRoute('change your Theme', 'system.themes_page')->toString()
        ]
      );

      $this->messenger->addError($message);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];

      return $form;
    }

    $form['query'] = [
      '#type' => 'radios',
      '#title' => $this->t('Query'),
      '#required' => true,
      '#options' => [
        'media' => '@media',
        'container' => '@container'
      ],
      '#default_value' => $this->configuration['query'],
      '#attributes' => [
        'class' => ['query']
      ]
    ];

    $form['breakpoints'] = [
      '#type' => 'fieldset',
      '#title' => "Layout Definition",
      '#attributes' => [
        'class' => ['layout-definition']
      ]
    ];

    foreach ($this->breakpoint_group as $breakpoint) {
      // Config key must not have dots. @see Drupal\Core\Config\ConfigValueException
      // -> Substitute '.' with with '/'
      $bp_key = str_replace('.', '/', $breakpoint->pluginId);
      $defaults = $this->configuration['breakpoints'][$bp_key] ?? null;

      $form['breakpoints'][$bp_key] = [
        '#type' => 'details',
        '#title' => [
          'title' => $this->getBreakpointTitle($breakpoint),
          'icon' => $this->getBreakpointIcon($breakpoint, $defaults)
        ],
        '#attributes' => [
          'class' => ['breakpoint']
        ],
      ];

      $form['breakpoints'][$bp_key]['areas'] = [
        '#type' => 'textarea',
        '#title' => self::area_label_plural,
        // '#description' => $this->t('Follow the css standard for grid-template-areas.'),
        '#default_value' => GenericLayoutHelper::joinAreas($defaults['areas'] ?? []),
        '#placeholder' => "title title\ntext image",
        '#cols' => 30,
        '#rows' => 4,
        '#attributes' => [
          'class' => ['areas']
        ]
      ];

      $areas_selector = "textarea[name=\"layout_settings[breakpoints][{$bp_key}][areas]\"]";

      $form['breakpoints'][$bp_key]['inline'] = [
        '#title' => $this->t("Horizontal"),
      ] + $this->getAxisFormItems('columns', 'justify', $defaults, $areas_selector);
      $form['breakpoints'][$bp_key]['block'] = [
        '#title' => $this->t("Vertical")
      ] + $this->getAxisFormItems('rows', 'align', $defaults, $areas_selector);
    }

    return $form;
  }

  static private function sanitizeValues(FormStateInterface &$form_state): void {
    $sanitized = [];
    foreach ($form_state->getValue('breakpoints') as $id => &$data) {
      if (empty($data['areas']))
        continue;

      $sanitized[$id] = array_filter($data['inline']) + array_filter($data['block']);
      $sanitized[$id]['areas'] = GenericLayoutHelper::splitAreas($data['areas']);
      // All regions of the breakpoint
      $sanitized[$id]['regions'] = array_values(array_unique(array_merge(...$sanitized[$id]['areas'])));

      foreach (self::GRID_AXES as $axis)
        if(isset($sanitized[$id][$axis]))
          $sanitized[$id][$axis] = GenericLayoutHelper::splitAxis($sanitized[$id][$axis]);
    }
    $form_state->setValue('breakpoints', $sanitized);

    // All regions of the layout
    $regions = array_unique(array_merge(...array_column($sanitized, 'regions')));
    $form_state->setValue('regions', $regions);
  }

  /**
   * @inheritDoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);

    self::sanitizeValues($form_state);
    foreach ($form_state->getValue('breakpoints') as $breakpoint_id => $data) {
      $row_columns = array_map(function ($row) {
        return count($row);
      }, $data['areas']);

      // # Validate Areas geometry
      if (!count(array_unique($row_columns)) === 1) {
        // @todo improve Error with these stats
        $areas_column_count_stats = array_count_values($row_columns);

        $form_state->setErrorByName(
          self::formItemReference($breakpoint_id, 'areas'),
          $this->t("All columns in <em>Areas</em> must share the same number of items.")
        );
      }

      // @todo dissalow diagonal area
      // E.g. this is invalid:
      // "text image"
      // "image details"

      foreach (self::GRID_AXES as $axis) {
        if(isset($data[$axis])) {
          $max = ($axis == 'columns') ? reset($row_columns) : count($data['areas']);

          // Validate Count
          if (count($data[$axis]) > $max) {
            $plural = ucfirst($axis);
            $singular = rtrim($plural, 's');

            $form_state->setErrorByName(
              self::formItemReference($breakpoint_id, $axis),
              $this->t(
                "That's @diff to much! Please provide 0 -> @max.",
                [
                  "@diff" => $this->formatPlural(count($data[$axis]) - $max, "1 {$singular}", "@count {$plural}"),
                  "@max" => $this->formatPlural($max, "1 {$singular}", "@count {$plural}")
                ]
              )
            );
          }
        }
      }
    }

    if (empty($form_state->getValue('regions'))) {
      $form_state->setErrorByName('breakpoints',
        $this->t("At least one <em>@area_label</em> must be defined!", ['@area_label' => self::area_label_singular])
      );
    }
  }

  /* Determines new, existing region for orphaned regions for migration */
  private static function simpleRegionMigration(array $from, array $to): array {
    $orphaned = array_diff($from, $to);
    $created = array_diff($to, $from);
    $presisted = array_intersect($from, $to);

    $migration = [];

    // Use persisted regions first
    if (count($created) < count($orphaned)) {
      array_push($migration, ...$presisted);
    }
    // fillup with created regions
    while (count($migration) < count($orphaned)) {
      array_push($migration, ...$created);
    }
    // Trim to same length
    $migration = array_slice($migration, 0, count($orphaned));
    
    return array_combine($orphaned, $migration);
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);    

    if (!$this->isNew()) {
      $css_changed = array_reduce(self::CONFIGS, function ($carry, $key) use ($form_state) {
          return $carry | $this->configuration[$key] != $form_state->getValue($key);
        },
        false
      );

      $regions_changed = $this->configuration['regions'] != $form_state->getValue('regions');

      if ($regions_changed) {
        $migration = self::simpleRegionMigration($this->configuration['regions'], $form_state->getValue('regions'));
        foreach ($migration as $from => $to) {
          /** @var \Drupal\layout_builder\Form\ConfigureSectionForm $form_object */
          $form_object = $form_state->getFormObject();

          foreach ($form_object->getCurrentSection()->getComponentsByRegion($from) as $component) {
            $component->setRegion($to);

            $this->messenger->addWarning($this->t("@name migrated: <em>@from</em> ⤳ <em>@to</em>", [
              '@name' => $component->toArray()['configuration']['label'],
              '@from' => $from,
              '@to' => $to
            ]));
          }
        }
      }
    }

    if ($this->isNew() || $regions_changed || $css_changed) {
      foreach (self::CONFIGS as $value) {
        $this->configuration[$value] = $form_state->getValue($value);
      }

      $this->configuration['regions'] = $form_state->getValue('regions');

      // array_map(function ($value) use ($form_state) {
      //   $this->configuration[$value] = $form_state->getValue($value);
      // }, self::CONFIGS + ['regions']);


      // No need to use \Drupal::service('uuid')->generate()
      // Uniqueness to this website is sufficient.
      // instance is prefixed to prevent int casts. Otherwise NestedArray::MergeDeep
      // applied later on it in LibraryDiscoveryParser casts it to 0
      $this->configuration['instance'] = "layout--" . uniqid();


      // Instead of nuking the whole libraries cache using \Drupal::service('library.discovery')->clearCachedDefinitions()
      // the cache for generic_layout alone can be cleared:
      $this->libraryDiscoveryCollector->delete('generic_layout');

      // Rebuilding generic_layout still takes some time, especially if you have many generic_layout's on your site.
      // @optimize A much faster implementation would discover and un-discover sole libraries - one by one!
      // However, the LibraryDiscoveryCollector's APIs seem not to support this yet.

      // One attempt for a more performat solution solution/workaround is sketched in \EventSubscriber\GenericLayoutSubscriber.
    }
  }

  // @todo Adjust nesting once CSS Nesting is mainstream
  // @see https://caniuse.com/css-nesting
  public function getCss(): Document {
    $document = new Document();

    foreach ($this->configuration['regions'] as $region) {
      $block = new DeclarationBlock();
      $block->setSelectors([new Selector("> .layout__region--{$region}")]);
      $document->append($block);

      $rule = new Rule('grid-area');
      $rule->addValue($region);
      $block->addRule($rule);
    }

    $regions_active = $this->configuration['regions'];
    foreach ($this->configuration['breakpoints'] as $id => $data) {
      $parent = $document;

      $breakpoint_id = str_replace('/', '.', $id);
      if ($media_query = $this->breakpoint_group[$breakpoint_id]->getMediaQuery()) {
        $query = new AtRuleBlockList(
          $this->configuration['query'],
          $this->configuration['query'] == 'container' ? strstr($media_query, '(') : $media_query
        );

        $query->addComments([new Comment(" breakpoint: $breakpoint_id ")]);

        $parent->append($query);
        $parent = $query;
      }

      // Grid Layout for breakpoint
      $block = new DeclarationBlock();
      $block->setSelectors([new Selector("")]);
      $parent->append($block);

      if ($parent == $document) {
        // display: grid;
        $rule = new Rule('display');
        $rule->addValue('grid');
        $block->addRule($rule);
      }

      if (!empty($data['areas'])) {
        $rule = new Rule('grid-template-areas');
        foreach ($data['areas'] as $row) {
          $rule->addValue([new CSSString(implode(' ', $row))]);
        }
        $block->addRule($rule);
      }

      foreach (self::GRID_AXES as $axis) {
        if (!empty($data[$axis])) {
          $rule = new Rule("grid-template-{$axis}");
          $value_list = new RuleValueList(' ');
          $value_list->setListComponents($data[$axis]);
          $rule->addValue($value_list);
          $block->addRule($rule);
        }
      }

      foreach (self::GRID_FLOW as $axis) {
        foreach (array_keys($this->grid_flow_values) as $set) {
          $property = "{$axis}_{$set}";
          if (!empty($data[$property])) {
            $rule = new Rule(str_replace('_', '-', $property));
            $rule->setValue($data[$property]);
            $block->addRule($rule);
          }
        }
      }

      // Region visibility
      $region_display = [
        'none' => array_diff($regions_active, $data['regions']),
        'block' => array_diff($data['regions'], $regions_active)
      ];
      foreach ($region_display as $value => $regions) {
        foreach ($regions as $region) {
          $block = new DeclarationBlock();
          $block->setSelectors([new Selector("> .layout__region--{$region}")]);
          $parent->append($block);

          $rule = new Rule('display');
          $rule->addValue($value);
          $block->addRule($rule);
        }
      }

      $regions_active = $data['regions'];
    }

    // @todo dispatch Event to allow users change generated $css?
    return $document;
  }

  public function getInstance(): string {
    return $this->configuration['instance'];
  }

  public function isNew(): bool {
    return !isset($this->configuration['instance']);
  }

  /**
   * @inheritDoc
   */
  public function build(array $regions): array {
    $instance = $this->configuration['instance'];

    $plugin_regions = [];
    foreach ($this->configuration['regions'] as $region) {
      $plugin_regions[$region]['label'] = $region;
    }
    $this->getPluginDefinition()->setRegions($plugin_regions);
    $this->getPluginDefinition()->setLibrary("generic_layout/$instance");

    $build = parent::build($regions);
    $build['#attributes']['class'][] = 'generic-layout';
    $build['#attributes']['class'][] = $instance;
    if ($label = $this->configuration['label']) {
      $build['#attributes']['id'] = strtolower(Html::cleanCssIdentifier($label));
    }

    return $build;
  }
}
