<?php
declare(strict_types=1);

namespace Drupal\generic_layout;

use Drupal\Core\Entity\EntityInterface;
use Drupal\layout_builder\SectionStorageInterface;

use Drupal\layout_builder\DefaultsSectionStorageInterface;
use Drupal\layout_builder\OverridesSectionStorageInterface;

class GenericLayoutHelper {
  public static function splitAreas(string $areas): array {
    $rows = explode("\r\n", $areas);
    return array_map(function ($row) {
      return explode(" ", $row);
    }, $rows);
  }

  public static function joinAreas($areas) {
    return implode("\r\n", array_map(function ($row) {
      return implode(' ', $row);
    }, $areas));
  }

  /**
   * Sabberworm CSS can't parse expressions like max(200px, 1fr) so here's a workaround:
   * @param mixed $input
   * @return null|array
   */
  public static function splitAxis(string|null $axis): array|null {
    if (empty($axis))
      return null;

    $value = '';
    $values = [];
    $level = 0;       // current nesting level

    // Cleanup additional whitespaces
    $axis = preg_replace('/\s+/', ' ', trim($axis));

    foreach (str_split($axis) as $char) {
      if ($char == ' ' && $level == 0) {
        $values[] = $value;
        $value = '';
        continue;
      }

      if ($char == '(')
        $level++;
      else if ($char == ')')
        $level--;

      $value .= $char;
    }
    $values[] = $value;

    return $values;
  }

  public static function extractGenericLayoutSections(array $sections): array {
    $gl_sections = array_filter(
      $sections,
      function ($section) {
        return $section->getLayoutId() == 'generic_layout';
      }
    );

    return $gl_sections;
  }

  public static function extractGenericLayoutInstances(array $gl_sections): array {
    return array_map(
      function ($section) {
        return $section->getLayout()->getInstance();
      },
      $gl_sections
    );
  }

  // @see https://www.drupal.org/project/drupal/issues/3279716
  public static function getEntityForSectionStorage(SectionStorageInterface $sectionStorage): EntityInterface|null {
    if ($sectionStorage instanceof DefaultsSectionStorageInterface) {
      /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
      $display = \Drupal::service('entity_type.manager')->getStorage('entity_view_display')->load($sectionStorage->getStorageId());
      return $display;
    }
    elseif ($sectionStorage instanceof OverridesSectionStorageInterface) {
      [$entity_type_id, $id] = explode('.', $sectionStorage->getStorageId());
      $entity = \Drupal::service('entity_type.manager')->getStorage($entity_type_id)->load($id);
      return $entity;
    }
    return null;
  }
}
