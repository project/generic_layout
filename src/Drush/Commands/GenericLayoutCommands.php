<?php
declare(strict_types=1);

namespace Drupal\generic_layout\Drush\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\generic_layout\GenericLayoutManager;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A Drush commandfile.
 */
class GenericLayoutCommands extends DrushCommands {
  use StringTranslationTrait;

  /**
   * Constructs a GenericLayoutCommands object.
   */
  public function __construct(
    protected readonly GenericLayoutManager $libraryManager
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('generic_layout.manager')
    );
  }

  /**
   * Clears all css files to force their recreation
   *
   * @command generic_layout:remove-css
   * @aliases glrc
   *
   * @usage generic_layout:regenerate
   *   In local environments, you may need to run as apache: "sudo -u apache drush generic_layout:regenerate"
   *
   */
  public function removeCssFiles(): void {
    $this->libraryManager->removeCssFiles();
    $this->logger()->success("generic_layout *.css files cleared.");
  }

  /**
   * Clears temporary stored Layouts, (those being edited but not yet saved)
   *
   * @command generic_layout:clear-tempstore
   * @aliases glct
   *
   * @usage generic_layout:clear-tempstore
   *
   */
  public function clearLayoutTempstores(): void {
    $count = $this->libraryManager->clearLayoutTempstoreRepositories();

    $this->logger()->success(
      (string) $this->formatPlural($count, "@count temporary layouts have been cleared.", "@count temporary layout has been cleared.")
    );
  }
}
