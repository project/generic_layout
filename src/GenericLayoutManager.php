<?php

declare(strict_types=1);

namespace Drupal\generic_layout;

use Drupal\Core\File\FileExists;

use Sabberworm\CSS\OutputFormat;
use Drupal\Core\File\FileSystemInterface;

use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\generic_layout\GenericLayoutHelper;
use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;

use Drupal\layout_builder\LayoutTempstoreRepository;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\generic_layout\Plugin\Layout\GenericLayout;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;


class GenericLayoutManager {
  use LayoutEntityHelperTrait;
  const DIRECTORY = "public://generic_layout";

  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityFieldManagerInterface $entityFieldManager,
    protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected readonly EntityDisplayRepositoryInterface $entityDisplayRepository,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly LibraryDiscoveryCollector $libraryDiscoveryCollector,
    protected readonly LayoutTempstoreRepository $layoutTempstoreRepository,
  ) {
  }

  public static function getCssUri(GenericLayout $layout): string {
    $subfolder = $layout->getPluginDefinition()->id();
    $uri = "public://{$subfolder}/{$layout->getInstance()}.css";

    return $uri;
  }

  public function getGenericLayoutSections(): array {
    $section_storage = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $info) {
      $entityStorage = $this->entityTypeManager->getStorage($entity_type_id);
      foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle => $bundle_info) {
        // Collect Defaults
        foreach(array_keys($this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle)) as $view_mode) {
          $display = $this->entityDisplayRepository->getViewDisplay($entity_type_id, $bundle, $view_mode);
          if($display instanceof LayoutBuilderEntityViewDisplay && $display->isLayoutBuilderEnabled()) {
            $section_storage[] = $this->sectionStorageManager()->load('defaults', ['display' => EntityContext::fromEntity($display)]);
            if($display->isOverridable()) {
              $entity_ids = $entityStorage->getQuery()->accessCheck(FALSE)->execute();
              foreach ($entityStorage->loadMultiple($entity_ids) as $entity) {
                // @return Null: entity uses default Layout / is not overriden
                if($ss = $this->getSectionStorageForEntity($entity, $view_mode)) {
                  $section_storage[] = $ss;
                }
              }
            }
          }
        }
      }
    }

    $sections = [];
    foreach($section_storage as $ss) {
      $sections = array_merge($sections, GenericLayoutHelper::extractGenericLayoutSections($ss->getSections()));

      if ($this->layoutTempstoreRepository->has($ss)) {
        $sst = $this->layoutTempstoreRepository->get($ss);
        $sections = array_merge($sections, GenericLayoutHelper::extractGenericLayoutSections($sst->getSections()));
      }
    }

    return $sections;
  }

  // @see: https://github.com/sabberworm/PHP-CSS-Parser#use-parser-to-prepend-an-id-to-all-selectors
  public static function prefixSelectors(string $prefix, \Sabberworm\CSS\CSSList\Document &$css) {
    foreach ($css->getAllDeclarationBlocks() as $block) {
      foreach ($block->getSelectors() as $selector) {
        $selector->setSelector($prefix . ' ' . $selector->getSelector());
      }
    }
  }

  public function storeCssFile(GenericLayout $layout): string {
    $instance = $layout->getInstance();
    $css_uri = self::getCssUri($layout);

    $css = $layout->getCss();
    self::prefixSelectors('.' . $instance, $css);
    $rendered_css = $css->render(OutputFormat::createPretty());

    $directory = self::DIRECTORY;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    return $this->fileSystem->saveData($rendered_css, $css_uri, FileExists::Replace);
  }

  public function removeCssFile(GenericLayout $layout): void {
    $css_uri = self::getCssUri($layout);
    $this->fileSystem->delete($css_uri);
  }

  public function removeAbandonedCssFiles(array $files_to_keep): void {
    $files = $this->fileSystem->scanDirectory(self::DIRECTORY, '/.*/', ['key' => 'name']);
    $abandoned_files = array_diff_key($files, $files_to_keep);

    foreach ($abandoned_files as $uri => $data) {
      $this->fileSystem->delete($data->uri);
    }
  }

  public function removeCssFiles() {
    // @todo whats the right one?
    // $this->fileSystem->rmdir(self::DIRECTORY);
    $this->fileSystem->deleteRecursive(self::DIRECTORY);

    \Drupal::service('library.discovery')->clearCachedDefinitions();
  }

  public function getLibrary(GenericLayout $layout): array {
    $css_uri = self::getCssUri($layout);

    if (!file_exists($css_uri)) {
      $this->storeCssFile($layout);
    }

    $library = [
      'css' => [
        'layout' => [
          $css_uri => [
            'preprocess' => false,
          ]
        ]
      ]
    ];

    return $library;
  }

  public function getLibraries(): array {
    $libraries = [];

    foreach ($this->getGenericLayoutSections() as $section) {
      $layout = $section->getLayout();
      $libraries[$layout->getInstance()] = self::getLibrary($layout);
    }

    // Now is a cheap and reliable moment to get rid of abandoned css files
    $this->removeAbandonedCssFiles($libraries);

    return $libraries;
  }

  /**
   * If the LayoutSettings scheme changes, this helper makes it easy, to update existing Layouts accordingly.
   * @todo due to the need of changing all Settings in normal operation (f.e. when changing the breakpoint group),
   * the function may be moved to GridLayoutManager
   * This is typically followed by a call of clearLayoutTempstoreRepositories() to also get rid of invalid configurations
   * in currenlty edited layouts
   *
   * 
   * Example removing depricated 'max_width' setting:
   *
   * \Drupal::service('generic_layout.manager')->transformLayoutSections( function($section) {
   *   $settings = $section->getLayoutSettings();
   *   unset($settings['max_width']);
   *   $section->setLayoutSettings($settings);
   * });
   *
   * 
   * Example, renaming ui_styles for any layout
   * 
   * function renameStyles(array $ui_styles): array {
   * 
   *   $replace = [
   *     "_top" => "_block_start",
   *     "_bottom" => "_block_end",
   *     "_left" => "_inline_start",
   *     "_right" => "_inline_end",
   *     "border_color" => "color_border",
   *     "background_color" => "color_background",
   *     "heading_color" => "color_heading",
   *     "background_gradient" => "color_background_gradient",
   *   ];
   * 
   *   foreach($ui_styles as $key => $value) {
   *     foreach($replace as $old => $new) {
   *       if(str_ends_with($key, $old)) {
   *         $ui_styles[str_replace($old, $new, $key)] = $value;
   *         unset($ui_styles[$key]);
   *       }
   *     }
   *   }
   * 
   *   return $ui_styles;
   * }
   * 
   * \Drupal::service('generic_layout.manager')->transformLayoutSections( function($section) {
   *   $ui_styles = $section->getThirdPartySetting('ui_styles', 'selected');
   *   $ui_styles = renameStyles($ui_styles);
   *   $section->setThirdPartySetting('ui_styles', 'selected', $ui_styles);
   *   
   *   foreach($section->getComponents() as $component) {
   *     $ui_style_keys = array_keys($component->toArray()['additional']);
   *     foreach($ui_style_keys as $key) {
   *       $ui_styles = $component->get($key);
   *       if(!empty($ui_styles) and is_array($ui_styles)) {
   *         $ui_styles = renameStyles($ui_styles);
   *         $component->set($key, $ui_styles);
   *       }
   *     }
   *   }
   * });
   * @param $callback
   * @return void
   */
  public function transformLayoutSections(callable $callable, bool $clear_cache = TRUE, bool $generic_layout_only = TRUE): void {
    foreach ($this->entityFieldManager->getFieldMapByFieldType('layout_section') as $entity_type_id => $data) {
      $entityStorage = $this->entityTypeManager->getStorage($entity_type_id);

      $ids = $entityStorage
        ->getQuery()
        ->accessCheck(FALSE)
        ->execute();

      foreach ($entityStorage->loadMultiple($ids) as $entity) {
        if ($section_storage = $this->getSectionStorageForEntity($entity)) {
          $sections = $section_storage->getSections();
          
          if($generic_layout_only) {
            $section = GenericLayoutHelper::extractGenericLayoutSections($sections);
          }

          // @todo use 'Drupal Batch API' cause a site may have dozens of Layouts
          // @see https://www.drupal.org/docs/drupal-apis/batch-api/batch-api-overview
          foreach ($sections as $section) {
            call_user_func($callable, $section);
          }
          $entity->save();
        }
      }
    }

    if ($clear_cache) {
      $this->libraryDiscoveryCollector->delete('generic_layout');
    }
  }

  public function updateStyles(callable $callable) {
    $this->transformLayoutSections( function($section) use($callable) {
      $ui_styles = $section->getThirdPartySetting('ui_styles', 'selected');
      $ui_styles = call_user_func($callable, $ui_styles);
      $section->setThirdPartySetting('ui_styles', 'selected', $ui_styles);
      
      foreach($section->getComponents() as $component) {
        $ui_style_keys = array_keys($component->toArray()['additional']);
        foreach($ui_style_keys as $key) {
          $ui_styles = $component->get($key);
          if(!empty($ui_styles) and is_array($ui_styles)) {
            $ui_styles = call_user_func($callable, $ui_styles);
            $component->set($key, $ui_styles);
          }
        }
      }
    }, FALSE, FALSE);
  }

  /**
   * Deletes all layout builder layouts in transitional edit state
   * Usefull to get rid of depricated/invalid/broken config keys
   */
  public function clearLayoutTempstoreRepositories(): int {
    // delete in database because an API solution does not exist.
    // See below code snippet
    $connection = \Drupal::service('database');
    $query = $connection->delete('key_value_expire')->condition('collection', 'tempstore.shared.layout_builder.section_storage.%', "LIKE");
    $count = $query->execute();

    return $count;

    // @see \Drupal\layout_builder\SectionStorageInterface->getStorageType()
    foreach(['defaults', 'overrides'] as $storage_type) {
      $collection = 'layout_builder.section_storage.' . $storage_type;
      $tempstore = \Drupal::service('tempstore.shared')->get($collection);

      // deleteAll API for tempstore does not exist
      // @see https://www.drupal.org/project/drupal/issues/2475719
      // $tempstore->deleteAll();
    }
  }
}
