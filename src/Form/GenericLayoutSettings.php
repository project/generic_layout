<?php
declare(strict_types=1);

namespace Drupal\generic_layout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure generic_layout settings for this site.
 */
class GenericLayoutSettings extends ConfigFormBase
{
  public function __construct(
    protected readonly BreakpointManagerInterface $breakpointManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('breakpoint.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'generic_layout_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['generic_layout.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form['breakpoint_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Breakpoint group'),
      '#options' => $this->breakpointManager->getGroups(),
      '#description' => $this->t('The Breakpoint group to provide in Generic Layouts.'),
      '#default_value' => $this->config('generic_layout.settings')->get('breakpoint_group'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $config = $this->config('generic_layout.settings');
    $breakpoint_group = $form_state->getValue('breakpoint_group');

    if($breakpoint_group != $config->get('breakpoint_group')) {
      \Drupal::service('generic_layout.manager')->updateLayoutSettings(function($settings){
        // @todo remap $settings['breakpoints'] to new $breakpoint_group
        $settings;
      });

      // Force re-generation of all generic_layout css files
      // \Drupal::service('generic_layout.manager')->removeCssFiles();
    }

    $config->set('breakpoint_group', $breakpoint_group)->save();
    parent::submitForm($form, $form_state);
  }
}
